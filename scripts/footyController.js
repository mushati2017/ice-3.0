 angular
	.module('footy')
	.controller('footyController',function($scope,footballdataFactory){
		var apiKey = 'e42cb6a6ecc949c8897e06d284a55e05';
		
		$scope.LeagueTable;
		$scope.currentTeamID=61;
		$scope.clickOn= 'con1'
		//$scope.UpdateTeam=function(con){
			
		//}
		
		
		$scope.getTeamID= function(string)
		{
			
			var s = string.split('/');
			var teamID =  s[s.length-1];	
			return teamID;
			
		}		
		
		$scope.updateTeam= function(object)
		{
			//console.log(object);
			
			$scope.clickOn= 'con2'
			updateChart();
			$scope.currentTeamID=object;
			footballdataFactory.getFixturesByTeam({
				id: $scope.currentTeamID,
				apiKey: apiKey,
			}).then(function(_data){
				//console.info("getFixturesByTeam", _data);
				$scope.TeamFixtures = _data.data.fixtures;
				$scope.FilterResults = function(item)
				{
					var d =  new Date(item.date);
					var current= new Date();
					if(d<current)
					{
						return item;
					}
				}
				$scope.FilterFixture = function(item)
				{
					var d =  new Date(item.date);
					var current= new Date();
					if(d>current)
					{
						return item;
					}
				}
				$scope.FilterLast5 = function(item)
				{
					var d =  new Date(item.date);
					var current= new Date();
					if(d<current)
					{
						return item;
					}
				}
				
			});
			
		}
		
		/*
		footyFactory.getData().then(function(data){
			
			$scope.hello= data;
			
		},function(error){
			console.log(error);
		});
		*/
		
		
		footballdataFactory.getLeagueTableBySeason({
			id: 426,
			matchday: 38,
			apiKey: apiKey,
		}).then(function(_data){
			console.info("getLeagueTableBySeason", _data.data.standing);
			$scope.LeagueTable= _data.data.standing;
		});
		
		$scope.Teams;
	    footballdataFactory.getTeamsBySeason({
			id: 426,
			apiKey: apiKey,
		}).then(function(_data){
			console.info("getTeamsBySeason", _data);
			$scope.Teams= _data.data.teams;
		});

		$scope.TeamFixtures;
		$scope.FixtureFilter;
		
		footballdataFactory.getFixturesByTeam({
			id: $scope.currentTeamID,
			apiKey: apiKey,
		}).then(function(_data){
			console.info("getFixturesByTeam", _data);
			$scope.TeamFixtures = _data.data.fixtures;
			$scope.FilterResults = function(item)
			{
				var d =  new Date(item.date);
				var current= new Date();
				if(d<current)
				{
					return item;
				}
			}
			$scope.FilterFixture = function(item)
			{
				var d =  new Date(item.date);
				var current= new Date();
				if(d>current)
				{
					return item;
				}
			}
			$scope.FilterLast5 = function(item)
			{
				var d =  new Date(item.date);
				var current= new Date();
				if(d<current)
				{
					return item;
				}
			}
 			
		});
		
		
		
		footballdataFactory.getPlayersByTeam({
			id : $scope.currentTeamID,
			apiKey: apiKey,
		}).then(function(_data){
			console.info("getPlayersByTeam", _data);
			$scope.TeamPlayers= _data.data.players	
		});
		
		
		
	});